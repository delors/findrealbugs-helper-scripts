#!/bin/sh
set -e

resultsdir=`findrealbugs-helper-scripts/make-output-name.sh`

mkdir -p "$resultsdir"
findrealbugs-helper-scripts/run-build-and-corpus.sh "$resultsdir" > "$resultsdir/test-log.txt" 2>&1
