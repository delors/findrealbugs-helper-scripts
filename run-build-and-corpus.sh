#!/bin/sh
set -e

resultsdir="$1"

echo "begin: `date`"

# Update FindRealBugs clone, run tests, rebuild the .jar
cd bat-findrealbugs
echo "Updating FindRealBugs clone... (git pull)"
git pull
echo "Running sbt to build, test & package FindRealBugs..."
~/findrealbugs-helper-scripts/run-sbt.sh clean copy-resources doc compile assembly
cd ..

# If output dir was given here, copy the .jar into it
if [ -n "$resultsdir" ]; then
    mkdir -p "$resultsdir"
    cp bat-findrealbugs/ext/findrealbugs/target/scala-2.10/Ext-FindRealBugs-assembly-snapshot.jar "$resultsdir"
fi

# Run the .jar on the Qualitas Corpus Systems
findrealbugs-helper-scripts/corpus.sh "" "$resultsdir"

echo "end: `date`"
