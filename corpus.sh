#!/bin/sh
set -e

# Location of the Qualitas Corpus' Systems directory
systemsdir="qualitas-corpus/QualitasCorpus-20130901r/Systems"

# Argument 1 = list of system names of systems to test (useful when testing a
# subset manually). Empty = test all.
if [ -n "$1" ]; then
    systems="$1"
    echo "Running FindRealBugs only on these Qualitas Corpus systems: $systems..."
else
    systems=`ls $systemsdir`
    echo "Running FindRealBugs on all Qualitas Corpus systems..."
fi

# Argument 2 = results directory.
if [ -n "$2" ]; then
    resultsdir="$2"
else
    resultsdir="corpus-results-`date '+%Y-%m-%d-%H-%M-%S'`"
fi
mkdir -p "$resultsdir"

# Use .jar from results directory if it exists, otherwise use the .jar from
# the bat-findrealbugs/ directory. This allows the .jar to be placed next to the
# test results, making it easy to reproduce the issues with the exact .jar.
localjar="$resultsdir/Ext-FindRealBugs-assembly-snapshot.jar"
globaljar="bat-findrealbugs/ext/findrealbugs/target/scala-2.10/Ext-FindRealBugs-assembly-snapshot.jar"
if [ -f "$localjar" ]; then
    foundjar="$localjar"
else
    foundjar="$globaljar"
fi
echo "Testing with '$foundjar'. Writing results into '$resultsdir'..."

total=`echo $systems | wc -w`
# Strip leading whitespace added by wc
total=`echo $total | sed -e 's/^[ \t]*//'`

count="0"

run_test() {
    count=`expr $count + 1`
    echo "($count/$total) Testing on $i..."

    # Run FindRealBugs on the system's directory. It will automatically scan
    # for .jars in that directory.
    outputfile="$resultsdir/$i.txt"
    if java -jar "$foundjar" "$systemsdir/$i" -i=UselessIncrementInReturn > "$outputfile" 2>&1; then
        echo "ok"
    else
        echo "crashed/aborted"
    fi

    linecount=`cat "$outputfile" | wc -l | sed -e 's/^[ \t]*//'`
    echo "$linecount lines of output"
}

for i in $systems; do
    echo "----------------------------------------"
    date '+%Y-%m-%d-%H-%M-%S'
    { time run_test; } 2>&1
done
